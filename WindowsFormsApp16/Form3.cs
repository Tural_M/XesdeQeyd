﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp16
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        public static List<Xesde> listXesde = new List<Xesde>();
        Xesde xesde;
        private void button1_Click(object sender, EventArgs e)
        {
            xesde.Name = textBox1.Text;
            xesde.Surname = textBox2.Text;
            xesde.Phone = maskedTextBox1.Text;
            xesde.Xesdelik = richTextBox1.Text;
            listXesde.Add(xesde);
            Hide();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            xesde = new Xesde();
        }
    }
    public class Xesde
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Xesdelik { get; set; }
    }
}
